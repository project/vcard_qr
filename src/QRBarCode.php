<?php

namespace Drupal\vcard_qr;

use chillerlan\QRCode\QRCode;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sabre\VObject\Component\VCard;

/**
 * QRBarCode - Barcode QR Code Image Generator.
 */
class QRBarCode {

  /**
   * Constructor for our class.
   */
  public function __construct(VCard $vcard, QRCode $qrcard) {
    $this->vcard = $vcard;
    $this->qrcard = $qrcard;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vcard_qr.VCard'),
      $container->get('vcard_qr.QRCode')
    );
  }

  /**
   * Write vcode on Method by library Vcard.
   *
   * @return string
   *   Return Qrcode base64.
   */
  public function qrCode($data) {
    // Delete unused variables.
    unset($this->vcard->_SERVICEID);
    unset($this->vcard->PRODID);
    unset($this->vcard->UID);
    // Explode Full name.
    $name = explode(' ', $data['name']);
    // Set name.
    $firstname = $name[0];
    $lastname = implode(' ', array_slice($name, 1));
    $this->vcard->add('N', [$lastname, $firstname, '', '', '', '']);
    if (!empty($data['company'])) {
      $this->vcard->add('ORG;TYPE=company', $data['company'], ['CHARSET' => 'UTF-8']);
    }
    if (!empty($data['job'])) {
      $this->vcard->add('TITLE', $data['job'], ['CHARSET' => 'UTF-8']);
    }
    if (!empty($data['phone'])) {
      $this->vcard->add('TEL;PREF;phone', $data['phone']);
    }
    $this->vcard->add('TEL;PREF;cell', $data['mobile']);
    if (!empty($data['email'])) {
      $this->vcard->add('EMAIL;TYPE=internet,pref', $data['email']);
    }
    return $this->qrcard->render($this->vcard->serialize());
  }

}
