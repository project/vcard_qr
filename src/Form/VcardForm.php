<?php

namespace Drupal\vcard_qr\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\vcard_qr\QRBarCode;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Vcard Form.
 */
class VcardForm extends FormBase {

  /**
   * Constructor for our class.
   */
  public function __construct(QRBarCode $qrBarCode) {
    $this->barCode = $qrBarCode;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vcard_qr.QRBarCode'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vcard_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $newqrcode = '';
    if (!empty($form_state->get('vcard'))) {
      $base64 = $this->barCode->qrCode($form_state->get('vcard'));
      $newqrcode = '<div id="vcard-qrcode-container"> <img src="data:' . $base64 . '" alt="QR Code" /> </div>' .
      '<a href="' . $base64 . '"  download="vcard.png" class="btn button button--primary">' . $this->t('Download') . '</a>';

      $form['vcard'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('QR code'),
        '#tree' => TRUE,
      ];
      $form['vcard']['qrcode'] = [
        '#type' => 'inline_template',
        '#template' => '{{ newqrcode|raw }}',
        '#context' => [
          'newqrcode' => $newqrcode,
        ],
      ];
    }

    $form['form'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Generated Vcard'),
      '#tree' => TRUE,
    ];
    $form['form']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#required' => TRUE,
    ];
    $form['form']['job'] = [
      '#type' => 'textfield',
      '#title' => $this->t('job'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['form']['mobile'] = [
      '#type' => 'tel',
      '#title' => $this->t('Mobile'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#pattern' => '[/(7|8|9)\d{9}/]*',
      '#required' => TRUE,
    ];
    $form['form']['phone'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#pattern' => '[/(7|8|9)\d{9}/]*',
    ];
    $form['form']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#weight' => '0',
    ];
    $form['form']['company'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $form_state->set('vcard', $values['form']);

    $form_state->setRebuild();
  }

}
